# Georgia Film Academy Redesign Project

A Front-end workflow environment with bootstrap 4.3.1, gulp 4, sass and BrowserSync using NPM and Gulp. Automatically compiles and minifies Bootstrap, fontawesome, jquery's node_modules files into the dist folder.

## Requirements

- [Node.js with NPM](https://nodejs.org/en/download/ "Node Js")
- Gulp

## Getting started

1. Clone repository:

- `git clone https://gitlab.galileo.usg.edu/jsteelio/georgia_film_academy.git`

2. Change directory:

- `georgia_film_academy`

3. Install all node modules:

- `npm install`

- Make sure you can run `gulp -v` and `npm -v`

4. Run Gulp Task:

- `gulp` - Starts localhost server with browser-sync, watches HTML, Sass, JS with hot reloading.

- `gulp build` - Compiles scss to css, minify css and js and builds apps ready for production into the **dist** folder.



## Overwriting Bootstrap Variable in Sass

- `georgia_film_academy/src/assets/scss/_bootstrap_variable_overrides.scss`

## Dependencies

```
  "devDependencies": {
    "gulp": "^4.0.0",
    "browser-sync": "^2.18.13",
    "gulp-sass": "^4.0.2",
    "gulp-clean": "^0.4.0",
    "panini": "^1.6.3",
    "gulp-sourcemaps": "^1.6.0",
    "gulp-imagemin": "^6.0.0",
    "gulp-cache": "^1.0.1",
    "run-sequence": "^2.2.0",
    "gulp-minify": "^3.1.0",
    "gulp-postcss": "^8.0.0",
    "gulp-autoprefixer": "^4.0.0",
    "merge-stream": "^1.0.1",
    "cssnano": "^4.1.10",
    "gulp-rename": "^1.4.0"
  },
  "dependencies": {
    "@fortawesome/fontawesome-free": "^5.8.1",
    "bootstrap": "^4.3.1",
    "jquery": "^3.3.1",
    "popper.js": "^1.15.0"
  }
```
