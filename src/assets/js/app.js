$( document ).ready(function() {
    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    // $('.dropdown-toggle').on('click', function () {
    //     $('a[aria-expanded=false]').addClass('up');
    // });
    // $('.collapsed').on('click', function () {
    //     $('a[aria-expanded=true]').removeClass('up');
    // }
    $('[data-toggle=collapse]').click(function(){
	
        // toggle icon
        $(this).find("i").toggleClass("fa-arrow-right fa-arrow-left");
    
  });
  new SimpleBar($('#sidebar')[0]);
  AOS.init({
    disable: 'mobile'
  });

  autoPlayVideoModal();
});

//FUNCTION TO GET AND AUTO PLAY VIDEO FROM DATATAG
function autoPlayVideoModal(){
  var trigger = $("body").find('[data-toggle="modal"]');
  trigger.click(function() {
    var video = document.querySelector("#video");
    var theModal = $(this).data( "target" );
    videoSRC = $(this).attr( "data-theVideo" ); 
    $(theModal+' video').attr('src', videoSRC);
    //autoplay when modal is opened 
    video.play();
    //stop video when x is clicked
    $(theModal+' button.close').click(function () {
        $(theModal+' video').attr('src', videoSRC);
    }); 
    //stop video when clicked outside modal
    $('#videoModal').on('hidden.bs.modal', function (e) {
      $('#videoModal .close').trigger('click')
      }); 
  });
}



